
var results = [];

document.querySelector('#cadastrar').addEventListener('click', () => {
    let price = replacePrice(document.querySelector('#price').value);
    let size = document.querySelector('#size').value;

    var obj = {
        name: document.querySelector('#name').value,
        size: size,
        price: price,
        pArea: calcPriceForArea(price, size),
        diference: ''
    }

    if (checkList(obj)) {
        alert('O tamanho informado já foi cadastrado!');
    } else if (validForm()) {
        alert('Preencha o(s) campo(s) vazio(s)!')
    } else {
        results.push(obj);
        alert('Pizza adicionada com suceeso.')
        // sortTable();
        calcDiference();
        renderTable();
    }
});

function sortTable() {
    results.sort((a, b) => a.pArea - b.pArea);
    // results[0].diference = 'Melhor CB';
    // calcDiference();
}

function renderTable() {
    var tbody = document.querySelector('#results');
    tbody.innerHTML = "";

    results.forEach(i => {
        let tr = document.createElement('tr');
        let tdName = document.createElement('td');
        let tdSize = document.createElement('td');
        let tdPrice = document.createElement('td');
        let tdPriceArea = document.createElement('td');
        let tdDiference = document.createElement('td');
        tr.appendChild(tdName);
        tdName.append(i.name);

        tr.appendChild(tdSize);
        tdSize.append(i.size + 'cm');

        tr.appendChild(tdPrice);
        tdPrice.append('R$ ');
        tdPrice.append(i.price);

        tr.appendChild(tdPriceArea);
        tdPriceArea.append('R$ ');
        tdPriceArea.append(i.pArea.toFixed(2));

        tr.appendChild(tdDiference);
        tdDiference.append(i.diference)
        tdDiference.append('%')

        tbody.appendChild(tr);
    });
}

function validForm() {
    return (document.querySelector('#price').value == '' || document.querySelector('#name').value == '' || document.querySelector('#size').value == '')
}

function checkList(obj) {
    return results.find(i => i.size == obj.size);
}

function replacePrice(price) {
    price = String(price).replace(',', '.');
    return Number.parseFloat(price).toFixed(2);
}

function calcDiference() {
    sortTable();
    results[0].diference = 'Melhor CB';

    console.log(results)
    
    for (let i = 0; i < results.length-1; i++) {
        let pizza1 = results[i].pArea;
        let pizza2 = results[i + 1].pArea;
        results[i+1].diference = (((pizza2 / pizza1) - 1) * 100).toFixed(2);
    }
}

function calcPriceForArea(price, size) {
    let raio = size / 2
    let area = 3.14 * (raio ** 2);
    return price / area
}


